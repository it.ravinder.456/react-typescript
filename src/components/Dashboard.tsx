import React, { useState } from "react";

interface Props {
  name: string;
  email: string;
  age: number;
}

export const Dashboard: React.FC<Props> = ({ name, email, age }) => {
  const [state, setstate] = useState<number | string>("Testing typescript");

  function cancatName(firstName: string, lastName?: string): string {
    return firstName + " " + lastName;
  }

  return (
    <div>
      {state} {cancatName("Ravinder")}
    </div>
  );
};
