import React from "react";
import { Dashboard } from "./components/Dashboard";

function App() {
  return (
    <React.Fragment>
      <Dashboard
        name={"Ravinder"}
        email={"it.ravinder.456@gmail.com"}
        age={12}
      />
    </React.Fragment>
  );
}

export default App;
